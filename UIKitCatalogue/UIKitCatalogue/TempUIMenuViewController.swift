//
//  TempUIMenuViewController.swift
//  UIKitCatalogue
//
//  Created by HOMWAY on 10/03/2017.
//  Copyright © 2017 ihomway. All rights reserved.
//

import UIKit

class TempUIMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		_addNotificationObeserver()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		_removeNotificationObeserver()
	}

	
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	//: 通过该方法，来判断哪些方法可以在 `UIMenuController` 中相应，不响应的不会出现在 UIMenuController 中
	override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
		
		var indx = 1
		
		if action == #selector(self.copy(_:))
		{
			return true
		}
		
		if action == #selector(self.paste(_:)) && indx % 2 == 0
		{
			return true
		}
		
		indx += 1
		
		return false
	}
	
	//: 返回 `true` 使得 `UIMenuController` 显示
	override var canBecomeFirstResponder: Bool
	{
		return true
	}
	
	@IBAction func _buttonAction(sender: UIButton) {
		
		let menuController = UIMenuController.shared
		menuController.setTargetRect(sender.frame, in: view)
		menuController.setMenuVisible(true, animated: true)
	}
	
	@IBAction func _updateMenu() {
		
		UIMenuController.shared.update()
	}
	
	func _addNotificationObeserver() {
		
		NotificationCenter.default.addObserver(self, selector: #selector(_receivedMenuViewControllerNotification(notifcation:)), name: Notification.Name.UIMenuControllerWillShowMenu, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(_receivedMenuViewControllerNotification(notifcation:)), name: Notification.Name.UIMenuControllerDidShowMenu, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(_receivedMenuViewControllerNotification(notifcation:)), name: Notification.Name.UIMenuControllerWillHideMenu, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(_receivedMenuViewControllerNotification(notifcation:)), name: Notification.Name.UIMenuControllerDidHideMenu, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(_receivedMenuViewControllerNotification(notifcation:)), name: Notification.Name.UIMenuControllerMenuFrameDidChange, object: nil)
		
	}
	
	func _removeNotificationObeserver() {
		
		NotificationCenter.default.removeObserver(self)
	}
	
	func _receivedMenuViewControllerNotification(notifcation: NSNotification) {
		
		print(notifcation)
		
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
